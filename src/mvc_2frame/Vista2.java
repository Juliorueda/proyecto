
package mvc_2frame;

import javax.swing.JButton;
import javax.swing.JLabel;

public class Vista2 extends javax.swing.JFrame {

    /**
     * Creates new form Vista2
     */
    public Vista2() {
        initComponents();
    }

  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        tel2 = new javax.swing.JLabel();
        nombre2 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton1.setText("OK");
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 210, 70, 70));

        tel2.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        tel2.setForeground(new java.awt.Color(204, 0, 51));
        getContentPane().add(tel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 300, 250, 90));

        nombre2.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        nombre2.setForeground(new java.awt.Color(255, 0, 51));
        getContentPane().add(nombre2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 200, 250, 90));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 255, 51));
        jLabel2.setText("Hola ! (=");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 40, -1, 70));

        jLabel1.setIcon(new javax.swing.ImageIcon("C:\\Users\\Acer\\Desktop\\FB_IMG_1458519938965.jpg")); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 480, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public JButton getjButton1() {
        return jButton1;
    }

    public JLabel getNombre2() {
        return nombre2;
    }

    public JLabel getTel2() {
        return tel2;
    }
    
  

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel nombre2;
    private javax.swing.JLabel tel2;
    // End of variables declaration//GEN-END:variables
}

package mvc_2frame;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Control_2 implements ActionListener{


private Vista2 vis2;
private int op=0;
private String nom2, tel2;

    public Control_2(Vista2 vis2, String nom2, String tel2) {
        this.vis2 = vis2;
        this.nom2 = nom2;
        this.tel2 = tel2;
    }
  

    public int getOp() {
        return op;
    }

    public String getNom2() {
        return nom2;
    }

    public String getTel2() {
        return tel2;
    }

    public void setOp(int op) {
        this.op = op;
    }

    public void setNom2(String nom2) {
        this.nom2 = nom2;
    }

    public void setTel2(String tel2) {
        this.tel2 = tel2;
    }
    

    public Control_2() { }

    public Control_2(Vista2 vis2) {
        this.vis2 = vis2;
       
    }

public void Visible()
{       if  (op !=1)   {
       vis2.setTitle(" Segunda Vista " );
       vis2.setLocationRelativeTo(null); 
       vis2.setVisible(true);
       vis2.getNombre2().setText("Nombre  " + nom2);
       vis2.getTel2().setText("Telefono  " + tel2);
       vis2.getjButton1().addActionListener(this);
         }  
  
  
    
}


    @Override
    public void actionPerformed(ActionEvent e) {
     if (e.getSource() == vis2.getjButton1())
     {
         
         System.exit(op);
     }
    }
}

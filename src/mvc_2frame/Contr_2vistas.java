
package mvc_2frame;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Contr_2vistas implements ActionListener{  
   // private Control_2  segundo;
    private Vista1_form v1;
    

    public Contr_2vistas( Vista1_form v1) {
        //this.segundo = segundo;
        this.v1 = v1;
    }

  
    
    public void Play()
    {
        v1.setTitle("Pantalla 1 Formulario");
        v1.setLocationRelativeTo(null);
        v1.setVisible(true);
        v1.getBoton_enviar().addActionListener(this);
       // segundo.Visible();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
      

if (e.getSource() == v1.getBoton_enviar())
{
    String nom=(v1.getNombre().getText());
    String tel=(v1.getTel().getText());
    v1.getNombre().setText("");
    v1.getTel().setText("");
     v1.setVisible(false);
 
     EventQueue.invokeLater(new Runnable()
          {
            public void run()
            {
               new Control_2(new Vista2(), nom, tel).Visible();
            }          
        }
    );

//segundo.Visible();
    //System.out.println(segundo.getTel2());
    //System.out.println(segundo.getNom2());
    
}

    }
    
}
